package com.snowplowanalytics

import java.util.Properties

import org.apache.flink.util.Collector
import org.apache.flink.api.scala._
import org.apache.flink.api.common.serialization._
import org.apache.flink.streaming.api.scala._
import org.apache.flink.streaming.connectors.kinesis._
import org.apache.flink.streaming.connectors.kinesis.config._
import org.apache.flink.streaming.api.functions.source.FileMonitoringFunction

import io.circe._, io.circe.generic.auto._, io.circe.parser._, io.circe.syntax._
import org.apache.flink.api.common.functions.RichFlatMapFunction
import org.apache.flink.api.common.typeinfo.TypeInformation
import model._
import model.implicits._

object model {
  case class BadRow(name: String, error: String)
  object implicits {
    implicit val badRowT = TypeInformation.of(classOf[BadRow])
    implicit val optionBadRowT = TypeInformation.of(classOf[Option[BadRow]])
    implicit val stringT = TypeInformation.of(classOf[String])
  }
}

object Job {
  def main(args: Array[String]): Unit = {
    val env = StreamExecutionEnvironment.getExecutionEnvironment

    val config = {
      val producerConfig = new Properties()
      producerConfig.put(AWSConfigConstants.AWS_REGION, "eu-central-1")
      producerConfig.put(AWSConfigConstants.AWS_ACCESS_KEY_ID, "access-key")
      producerConfig.put(AWSConfigConstants.AWS_SECRET_ACCESS_KEY, "secret-key")
      producerConfig.put(AWSConfigConstants.AWS_ENDPOINT, "https://host.docker.internal:4568")
      producerConfig.put("KinesisEndpoint", "host.docker.internal")
      producerConfig.put("KinesisPort", "4568")
      producerConfig.put("VerifyCertificate", "false")
      producerConfig
    }

    val kinesis = {
      val producer = new FlinkKinesisProducer[String](new SimpleStringSchema, config)
      producer.setFailOnError(true)
      producer.setDefaultStream("recovered")
      producer.setDefaultPartition("0")
      producer
    }

    val lines = env
      .readFileStream("s3://bad-rows/")
      .flatMap(FlatMapDeserialize)
      .filter(_.isDefined)
      .map(_.asJson.noSpaces)

    lines
      .addSink(kinesis)

    lines
      .print()

    env.execute("Flink Scala API Skeleton")
  }
}

object FlatMapDeserialize extends RichFlatMapFunction[String, Option[BadRow]] {
  override def flatMap(str: String, out: Collector[Option[BadRow]]): Unit = {
    out.collect(badRow(str))
  }
  //2.11
  def badRow(v: String) = decode[BadRow](v) match {
    case Right(a) => Some(a)
    case Left(err) => None
  }

}
